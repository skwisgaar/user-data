"use strict";

(function() {

    angular
        .module('test', ['ngRoute', 'ngAria', 'ngAnimate', 'ngMaterial'])
        .config(['$routeProvider', '$locationProvider', '$mdThemingProvider',
            function($routeProvider, $locationProvider, $mdThemingProvider) {
                $mdThemingProvider.theme('default')
                    .primaryPalette('indigo')
                    .accentPalette('orange');
                $routeProvider
                    .when('/', {
                        templateUrl: '/gmail.html',
                        controller: 'IndexCtrl'
                    })
                    .when('/hotmail', {
                        templateUrl: '/ms.html',
                        controller: 'IndexCtrl'
                    })
                    .when('/yahoo', {
                        templateUrl: '/yahoo.html',
                        controller: 'IndexCtrl'
                    })
                    .otherwise('/');
                $locationProvider.html5Mode(true);
            }
        ])
        .controller('IndexCtrl', ['$scope', '$location', 'sendJson',
            function($scope, $location, sendJson) {
                $scope.userData = {};
            }
        ])
        .factory('sendJson', ['$http',
            function($http) {
                return {
                    send: function(url, data) {
                        $http({
                            method: 'POST',
                            url: url,
                            headers: {
                                'Content-Type': "application/json"
                            },
                            data: data
                        })
                            .then(function(resp) {
                                console.log(resp);
                                document.getElementsByClassName('save')[0].style.display = "none";
                                document.getElementById('status').innerHTML = "Saved";
                            }, function(resp) {
                                document.getElementById('status').innerHTML = "Error on saving";
                            });
                    }
                }
            }
        ])
        .service('googleService', ['$http', '$rootScope', '$q',
            function($http, $rootScope, $q) {
                var clientId = '335447139738-0rqg617qto212ei53vmsn8v2hq85dmbv.apps.googleusercontent.com',
                    scopes = ['https://www.googleapis.com/auth/gmail.readonly'],
                    deferred = $q.defer();

                this.login = function() {
                    gapi.auth.authorize({
                        client_id: clientId,
                        scope: scopes,
                        immediate: false
                    }, this.handleAuthResult);

                    return deferred.promise;
                }

                this.handleAuthResult = function(authResult) {
                    if (authResult && !authResult.error) {
                        var data = {};

                        gapi.client.load('gmail', 'v1', function() {
                            var request = gapi.client.gmail.users.getProfile({
                                'userId': 'me'
                            });

                            request.execute(function(resp) {
                                var data = resp;
                                deferred.resolve(data);
                            });
                        });

                    } else {
                        deferred.reject('error');
                    }
                };
            }
        ])
        .service('msService', ['$http', '$rootScope', '$q',
            function($http, $rootScope, $q) {
                var client_id = '000000004417198E',
                    scopes = ["wl.basic", "wl.contacts_emails"],
                    deferred = $q.defer();

                this.init = function() {
                    WL.init({
                        client_id: client_id,
                        redirect_uri: 'http://skwisgaar.bitbucket.org',
                        scope: scopes
                    })
                }

                this.login = function() {

                    return WL.login({
                        scope: scopes
                    });
                    
                };
            }
        ])
        .directive('authGoogleMail', ['googleService', 'sendJson',
            function(googleService, sendJson) {
                function link(scope, element, attrs) {
                    element.bind('click', function(e) {
                        var that = this;
                        googleService.login().then(function(data) {
                            scope.userData = data.result;

                            that.style.display = "none";
                            document.getElementsByClassName('save')[0].style.display = "inline-block";

                        }, function(err) {
                            console.log('Failed: ' + err);
                        });
                    });
                }

                return {
                    link: link
                }
            }
        ])
        .directive('authMs', ['msService', 'sendJson',
            function(msService, sendJson) {
                function link(scope, element, attrs) {
                    element.bind('click', function(e) {
                        var that = this;
                        msService.init();
                        msService.login()
                        .then(function(response) {
                                WL.api({
                                    path: "me",
                                    method: "GET"
                                }).then(
                                    function(response) {
                                        console.log(response);
                                        scope.userData = response;
                                        scope.$apply();
                                        that.style.display = "none";
                                        document.getElementsByClassName('save')[0].style.display = "inline-block";
                                    },
                                    function(responseFailed) {
                                        console.log(responseFailed);
                                    }
                                );

                            },
                            function(responseFailed) {
                                deferred.reject('error');
                        });
                    });
                }

                return {
                    link: link
                }
            }
        ])
        .directive('sendData', ['sendJson',
            function(sendJson) {
                function link(scope, element, attrs) {
                    element.bind('click', function(e) {
                        sendJson.send('/data', scope.userData);
                    });
                }

                return {
                    link: link
                }
            }
        ]);
}());