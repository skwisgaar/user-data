
var express = require('express');
var app = express();

app.use('/', express.static(__dirname));

app.get('/[^\.]+$', function(req, res){
    res.set('Content-Type', 'text/html')
        .sendfile(__dirname + '/index.html');
});
app.post('/data', function(request, response){
	var fullBody = '';
	request.on('data', function(chunk){
		fullBody += chunk.toString();
	})
	request.on('end', function() {
		var fs = require('fs');
		
		fs.writeFile("data.json", fullBody);
		response.writeHead(200, "OK");
		response.end();
    });
	
});

app.listen(5555);